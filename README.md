# Description
This project is a benchmark method for the MILC Lattice QCD code
(https://github.com/milc-qcd/milc_qcd).
The methodology was developed by NERSC, in conjunction with the MILC developers,
in support of platform procurements.
But the benchmark can be used for a variety of benchmarking purposes.

MILC represents part of a set of codes written by the MIMD
Lattice Computation (MILC) collaboration used to study quantum chromodynamics
(QCD), the theory of the strong interactions of subatomic physics. It performs
simulations of four dimensional SU(3) lattice gauge theory on MIMD parallel
machines. "Strong interactions" are responsible for binding quarks into protons
and neutrons and holding them all together in the atomic nucleus. QCD discretizes
space and evaluates field variables on sites and links of a regular hypercube
lattice in four-dimensional space time. Each link between nearest neighbors in
this lattice is associated with a 3-dimensional SU(3) complex matrix for a given field.

The MILC collaboration has produced application codes to study several different
QCD research areas, only one of which is used here, `ks_imp_rhmc/su3_rhmd_hisq`.
This code generates lattices using rational function approximations for the
fermion determinants using the Rational Hybrid Monte Carlo (RHMC) algorithm and
implementing the HISQ action.

This project assumes the user of this benchmark is
already familiar with building `ks_imp_rhmc/su3_rhmd_hisq`.

# Benchmark
An example benchmark problem can be found in the `./test` directory.
The primary script used to set up a problem and execute it is `run_milc.sh`
This script should be modified to fit your environment and the specifics of the
target processor. As distributed, it uses the NERSC Slurm batch scheduler and
runtime for the Haswell partition of NERSC's Cori platform.
You'll need to modify it to fit your environment.

`Run_milc.sh` depends on other other scripts and input lattice files in the `./test`
directory.  In general, all files can be modified to suit your environment and needs.
However, with the exception of the target lattice size all input parameters as specified
in `milc_in.h` should NOT be modified in order to maintain a consistent basis for
comparison of results over time.

This distribution also provides an alternative
parameter input file called `milc_in_brief.sh` that can be used for benchmarking
in situations that don't require long runtimes. The "brief" input deck may be
desirable for all but the most extensive benchmarking and testing such as
machine acceptance. However, it should be noted that in production MILC spends
the majority of its time in the conjugate gradient solver phase, and with the "brief"
input deck this phase may NOT seem as significant in comparison to other phases and total runtime.
However, if you are interested in GF/s as a metric then it is representative as the processing
rate is the same.
The "brief" input deck can be used by specifying `--brief` flag to `run_milc.sh`.

In order to ensure that a given lattice is in a reasonably equilibrated state it
is necessary to run several warmup phases, which can take tens of minutes to hours.
This warmup is of no interest in benchmarking. Hence, lattices have been pre-generated
for all problems sizes and can be found in the `benchmarks/lattices` directory.
The provided run scripts are set up to load the appropriate lattice file.
Lattice files are very large, and not contained within this project.
See the README in `./lattices` to see how to download NERSC pre-generated lattice files.

If you would like to create a lattice of different dimensions for your own purposes,
the `run_milc.sh` script has a `--build` option.

# Usage and how to report performance
It is assumed that the user is familiar with building MILC applications and has already successfully built the application `su3_rhmd_hisq`.
In order to print timing and FLOP rate reports based on
values reported by MILC, your build needs to be
compiled with the following definitions in the Makefile.

    CTIME = -DCGTIME -DFFTIME -DFLTIME -DGFTIME

This project assumes it is installed in the MILC directory structure:

    cori03:src$ cd milc_qcd
    cori03:milc_qcd$ git clone https://gitlab.com/NERSC/milc-benchmark.git
    cori03:milc_qcd$ cd milc-benchmark/test

As distributed, `run_milc.sh` is configured for the NERSC Cori Haswell platform. It may need to be modified to suit your environment and runtime.

    cori03:test$ sbatch run_milc.sh --brief
    Submitted batch job 28742158
    cori03:test$

The script `parse-milc.sh` is used to report timing and FLOP rates. An example invocation is:

    cori03:test$ ../bin/parse-milc.sh slurm-28742158.out
    Parsing slurm-28742158.out
    Total(sec)  CG(sec)     FF(sec)     GF(sec)     CG(GF/s)    FF(GF/s)    GF(GF/s)
    63.609      27.956      28.121      4.481       24.669      20.221      26.852
    cori03:test$

Use `parse-milc.sh -h` to show general usage:

    cori03:test$ ../bin/parse-milc.sh -h
    ../bin/parse-milc.sh [-h] [-v] [-t threshold] [-f] file1 [file2 file3 ...]
    -h: prints this message
    -v: turns on verbosity, default is false
    -t threshold: only count if solver iterations > threshold, default is 0
    -f: forces an output even if run doesn't complete, default is false
    cori03:test$

# How to verify results
Verification is difficult, and there is no one value in the output that you can check
for correctness of the result. If a variant of the code is made, or the parallel
decomposition is changed for a given lattice size, there may be differences due to
different orders of floating-point operations that will contribute to variances in the
results.
However, the final values of "PLAQUETTE ACTION" and "CHECK: delta S" should
be close. You can also view the "PBP: mass" report, but note that the last two fields
are especially sensitive to roundoff.
The NERSC benchmark evaluation team will work with the MILC developers if
necessary to ensure a valid result.
