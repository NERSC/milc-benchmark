#!/bin/bash
# Script to parse the output of MILC
#

# Parse the command line parameters
verbose=false
iter_thresh=0
force_result=false
while getopts "hvt:f" opt; do
  case $opt in 
    h) 
      echo "$0 [-h] [-v] [-t threshold] [-f] file1 [file2 file3 ...]"
      echo "-h: prints this message"
      echo "-v: turns on verbosity, default is $verbose"
      echo "-t threshold: only count if solver iterations > threshold, default is $iter_thresh"
      echo "-f: forces an output even if run doesn't complete, default is $force_result"
      exit 0;;
    v) 
      verbose=true;;
    t) 
      iter_thresh=$OPTARG;;
    f) 
      force_result=true;;
    :)
      echo "Option -$OPTARG requires an arument" >&2; exit;;
    \?)
      echo "ERROR: Invalid option: -$OPTARG" >&2; exit;;
  esac
done
shift $((OPTIND -1))
if $verbose; then echo "Iteration threshold > $iter_thresh iters"; fi

# The remaining command line is a list of files to be parsed
first_time=true;
files_parsed=0
echo "Parsing $@"
for file in $@; do
  if [ -e $file ]; then
    exec < $file
    ((files_parsed++))
  else
    echo "***WARNING***: $file doesn't exist" >&2
    continue
  fi

  start=false
  i=0;
  while read -a fields; do
    if [ ${#fields[@]} -eq 0 ]; then # empy line
      continue
    fi
    case ${fields[0]} in
      "Machine") # Number of MPI ranks
        num_ranks[$i]=${fields[5]}
        if $verbose; then echo "${num_ranks[$i]} MPI ranks"; fi
        ;;
      "WARMUPS") # WARMUPS COMPLETED
        if $verbose; then echo "WARMUPS COMPLETED, starting calculation"; fi
        start=true
        tt_time[$i]=0
        cg_time[$i]=0
        ff_time[$i]=0
        gf_time[$i]=0
        mem[$i]=0
        cg_flops[$i]=0; cg_iters[$i]=0
        gf_flops[$i]=0; gf_iters[$i]=0
        ff_flops[$i]=0; ff_iters[$i]=0
        ;;
      "RUNNING") # RUNNING COMPLETED
        if $verbose; then echo "RUNNING COMPLETED, stopping calculation"; fi
        start=false
        ;;
      "Time") # Total time
        if [ "${fields[1]}" == "=" ] && [ "${cg_time[$i]}" != "0" ]; then
          time=`awk -v a="${fields[2]}" 'BEGIN{print (a * 1)}'`
          tt_time[$i]=`echo "${tt_time[$i]} + $time" | bc`
        fi
        ;;
      "exit:") # End of run
          (( i++ ));;
    esac

    if $start; then
      case ${fields[0]} in
        "CONGRAD5:") # CG FLOPS 
          if $verbose; then echo "${fields[@]}"; fi
          # time
          time=`awk -v a="${fields[3]}" 'BEGIN{print (a * 1)}'`
          cg_time[$i]=`echo "${cg_time[$i]} + $time" | bc`
          # iterations
          iters=${fields[11]}
          # FLOP/s
          if [ $iters -gt $iter_thresh ]; then
            flops=`awk -v a="${fields[14]}" 'BEGIN{print (a * 1)}'`
            cg_flops[$i]=`echo "${cg_flops[$i]} + $iters * $flops" | bc`
            (( cg_iters[$i] += iters ))
            tmp=`echo "${num_ranks[$i]} * ${cg_flops[$i]} / ${cg_iters[$i]} / 1000" | bc -l`
            if $verbose; then echo "${fields[@]}; GFLOPS=$tmp"; fi
          fi
          ;;
        "GFTIME:") # GF FLOPS 
          if $verbose; then echo "${fields[@]}"; fi
          # time
          time=`awk -v a="${fields[3]}" 'BEGIN{print (a * 1)}'`
          gf_time[$i]=`echo "${gf_time[$i]} + $time" | bc`
          # FLOP/s
          if [ "${fields[4]}" == "(QPhiX" ]; then 
            flops=`awk -v a="${fields[8]}" 'BEGIN{print (a * 1)}'`
          elif [ "${fields[4]}" == "(Symanzik1_QUDA)" ]; then 
            flops=`awk -v a="${fields[7]}" 'BEGIN{print (a * 1)}'`
          else
            flops=`awk -v a="${fields[7]}" 'BEGIN{print (a * 1)}'`
          fi
          gf_flops[$i]=`echo "${gf_flops[$i]} + $flops" | bc`
          # iterations
          (( gf_iters[$i] += 1 ))
          ;;
        "FFTIME:") # FF FLOPS 
          if $verbose; then echo "${fields[@]}"; fi
          # time
          time=`awk -v a="${fields[3]}" 'BEGIN{print (a * 1)}'`
          ff_time[$i]=`echo "${ff_time[$i]} + $time" | bc`
          # FLOP/s
          if [ "${fields[5]}" == "MILC" ]; then 
            flops=`awk -v a="${fields[15]}" 'BEGIN{print (a * 1)}'`
          else
            flops=`awk -v a="${fields[14]}" 'BEGIN{print (a * 1)}'`
          fi
          ff_flops[$i]=`echo "${ff_flops[$i]} + $flops" | bc`
          # iterations
          (( ff_iters[$i] += 1 ))
          ;;
      esac
    fi
  done # end of reading fields


  # print results
  if $first_time; then
    printf "%-12s%-12s%-12s%-12s%-12s%-12s%-12s%-12s\n" \
      "Total(sec)" "CG(sec)" "FF(sec)" "GF(sec)" \
      "CG(GF/s)" "FF(GF/s)" "GF(GF/s)"
    first_time=false
  fi

  # if we have an incomplete run, -f will force printing results that were found
  if $force_result && $start; then
    (( i += 1 ))
  fi
 
  for ((j=0; j < i; j++)); do
    if [ ${cg_iters[$j]} -gt 0 ] || [ ${gf_iters[$j]} -gt 0 ] || [ ${ff_iters[$j]} -gt 0 ]; then
      if [ ${cg_iters[$j]} -gt 0 ]; then
        cg_flops[$j]=`echo "${num_ranks[$j]} * ${cg_flops[$j]} / ${cg_iters[$j]} / 1000" | bc -l`
      fi
      if [ ${gf_iters[$j]} -gt 0 ]; then
        gf_flops[$j]=`echo "${num_ranks[$j]} * ${gf_flops[$j]} / ${gf_iters[$j]} / 1000" | bc -l`
      fi
      if [ ${ff_iters[$j]} -gt 0 ]; then
        ff_flops[$j]=`echo "${num_ranks[$j]} * ${ff_flops[$j]} / ${ff_iters[$j]} / 1000" | bc -l`
      fi
      printf "%-12.3f%-12.3f%-12.3f%-12.3f%-12.3f%-12.3f%-12.3f\n" \
        ${tt_time[$j]} ${cg_time[$j]} ${ff_time[$j]} ${gf_time[$j]} \
        ${cg_flops[$j]} ${ff_flops[$j]} ${gf_flops[$j]}
    fi
  done

done # end of reading files

