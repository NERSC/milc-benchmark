#!/bin/bash -l
#SBATCH -C gpu
#SBATCH -t 00:30:00
#SBATCH -J MILC_GPU
#SBATCH -o MILC_GPU.o%j
#SBATCH -A nstaff
#SBATCH -n 16
#SBATCH --cpus-per-task=10
#SBATCH --ntasks-per-node=4
#SBATCH --gpus-per-task=1

num_nodes=$SLURM_JOB_NUM_NODES
c_node=$SLURM_CPUS_ON_NODE
num_nodes=$SLURM_JOB_NUM_NODES
ntasks_node=$SLURM_NTASKS_PER_NODE
cpus_node=$SLURM_CPUS_ON_NODE
ht_node=2				# hyperthreads per node
c_task=$(( $cpus_node / $ntasks_node ))	# number of cores/node
gpu_task=$SLURM_GPUS_PER_TASK
total_gpus=$(( $gpu_task * $ntasks_node * $num_nodes))
ht_core=2			# number of hyperthreads per core

echo " gpus-per-tas = $gpu_task, ntask-node = $ntasks_node, num_nodes = $num_nodes "
echo " Total GPUs = $total_gpus "
nvshmem=1
export NVSHMEM_INFO=1

if [ ${nvshmem} == 1 ]
then
    module use /global/project/projectdirs/m888/tgroves/gpus/modules/
    module load nvshmem/2.0.3
else
    module rm gcc
    module load cuda/11.1.1
    module load gcc/8.3.0
    module load openmpi/4.0.5
fi

# Set defalt problem size
nx=32
ny=32
nz=32
nt=64

if [[ ${SLURM_JOB_PARTITION} == "dgx" ]]
then
export QUDA_RESOURCE_PATH=$PWD/qudatune_a100_"$nx"_"$ny"_"$nz"_"$nt"_$total_gpus
else
    if [ ${nvshmem} == 1 ]
    then
        export QUDA_RESOURCE_PATH=$PWD/qudatune_v100_nvshmem_"$nx"_"$ny"_"$nz"_"$nt"_"$total_gpus"GPUs
    else
        export QUDA_RESOURCE_PATH=$PWD/qudatune_v100_"$nx"_"$ny"_"$nz"_"$nt"_"$total_gpus"GPUs
    fi
fi

if [ ! -d $QUDA_RESOURCE_PATH ]; then
    mkdir $QUDA_RESOURCE_PATH
fi

if [[ ${SLURM_JOB_PARTITION} == "dgx" ]]
then
    exe=./su3_rhmd_hisq_a100
else
    if [ ${nvshmem} == 1 ] 
    then
        exe=./su3_rhmd_hisq_nvshmem_v100
    else
        exe=./su3_rhmd_hisq_v100
    fi
fi

ihelp=false
build=false
debug=false
brief=true
r=$SLURM_NTASKS_PER_NODE			# default number of ranks per node
t=0			# derive threads
while [ "$#" -ge 1 ] ; do
  case "$1" in
    "-h" | "--help" )     ihelp=true; shift 1;;
    "--exe" )             exe=$2; shift 2;;
    "--build" )           build=true; shift 1;;
    "--debug" )           debug=true; shift 1;;
    "--brief" )           brief=true; shift 1;;
    "--full" )            brief=false; shift 1;;
    "-r" | "--ranks" )    r=$2; shift 2;;
    "-t" | "--threads" )  t=$2; shift 2;;
    "-nx" )  		  nx=$2; shift 2;;
    "-ny" )  		  ny=$2; shift 2;;
    "-nz" )  		  nz=$2; shift 2;;
    "-nt" )  		  nt=$2; shift 2;;
    *)                    shift 1; continue;;
  esac
done

if $ihelp; then
  echo "$0 [--help] [--exe executable] [--build] [--debug] [--brief(default) | --full] \
[-r ranks] [-t threads/rank] [-nx nx] [-ny ny] [-nz nz] [-nt nt]"
  exit
fi

dimension=${nx}x${ny}x${nz}x${nt}
checklat=$dimension.chklat
if $build; then
  . milc_in.sh
  warms=40
  trajecs=0
  #save_cmd="save_parallel $checklat" # I've had issues with save_parallel on Cori
  save_cmd="save_serial $checklat"
  reload_cmd="continue"
  echo "$0: Building lattice of dimension $dimension"
else
  if $brief; then
    echo "$0: USING milc_in_brief.sh"
    . milc_in_brief.sh
  else
    . milc_in.sh
  fi
  warms=0
  trajecs=2
  save_cmd="forget"
  reload_cmd="reload_parallel $checklat"
  echo    "$0: Running MILC with lattice of dimension $dimension"
fi

#Determine number of tasks
n=$SLURM_NTASKS
#n=$(( r * num_nodes ))
c=$(( c_node / r ))     # cores per rank
if [ $t -eq 0 ]; then		# t not set on command line
  t=$(( c / ht_core ))
fi
export OMP_PROC_BIND=spread
export OMP_PLACES=threads

export EXE=$exe

if [[ ${SLURM_JOB_PARTITION} == "dgx" ]]
then
  OPTION=3
  if [[ ${OPTION} == "0" ]]; then
    # Rank to numa domain is reordered to minize distance to the GPU
    command="srun -n$n -c$c_task --cpu_bind=map_ldom:3,2,1,0,7,6,5,4 $exe"
  elif [[ ${OPTION} == "1" ]]; then
    # Only use numa domains 3, 1, 7 and 5 to avoid Infinity Fabric hops
    # Reduce the number of threads, and unset OMP bindings
    #   this means the OS can float cores and hopefully balance them w/o overlap
    export OMP_NUM_THREADS=8
    unset OMP_PROC_BIND
    unset OMP_PLACES
    command="srun -n$n -c$c_task --cpu_bind=map_ldom:3,3,1,1,7,7,5,5 $exe"
  elif [[ ${OPTION} == "2" ]]; then
    # Only use numa domains 3, 1, 7 and 5 to avoid Infinity Fabric hops
    # Explicity bind 1/2 the threads per domain
    #   should be an advantage over option 1
    export OMP_NUM_THREADS=8
    mask7h="0xff000000000000000000000000000000"
    mask7l="0x00ff0000000000000000000000000000"
    mask5h="0x00000000ff0000000000000000000000"
    mask5l="0x0000000000ff00000000000000000000"
    mask3h="0x0000000000000000ff00000000000000"
    mask3l="0x000000000000000000ff000000000000"
    mask1h="0x000000000000000000000000ff000000"
    mask1l="0x00000000000000000000000000ff0000"
    export mask="$mask3h,$mask3l,$mask1h,$mask1l,$mask7h,$mask7l,$mask5h,$mask5l"
    command="srun -n$n -c$c_task --cpu_bind=mask_cpu:$mask $exe"
    else
        export OMP_NUM_THREADS=16
        command="srun -n$n ./bind-cori-a100.sh"
    fi
else
export OMP_NUM_THREADS=10
export OMP_PROC_BIND=spread
export OMP_PLACES=cores
command="srun -n$n --cpu_bind=cores $EXE "
#command="srun -n$n --cpu_bind=map_ldom:0,0,0,0,1,1,1,1 $EXE "
fi

echo -n "OMP_PLACES=$OMP_PLACES; "
echo -n "OMP_PROC_BIND=$OMP_PROC_BIND; "
echo -n "OMP_NUM_THREADS=$OMP_NUM_THREADS; "


echo "$command"
$command

if $debug; then exit; fi
echo -n "$0: BEGIN TIME: "; date
run_milc
echo -n "$0: END TIME: "; date
