#!/bin/bash
#SBATCH -C gpu
#SBATCH -t 01:00:00
#SBATCH -J QUDA_MILC
#SBATCH -o MILC_GPU.o%j
#SBATCH -A mp13_g
#SBATCH -n 4
#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-task=32
#SBATCH --gpus-per-task=1
#SBATCH --gpu-bind=none

total_tasks=$SLURM_NTASKS
total_gpus=$( $SLURM_GPUS_PER_TASK * $total_tasks )

module load PrgEnv-gnu
module load cudatoolkit
module load craype-accel-nvidia80
export MPICH_GPU_SUPPORT_ENABLED=1

printf "%s\n" "total_tasks = $SLURM_NTASKS total_gpus = $total_gpus"

# Set defalt problem size
nx=36 
ny=36
nz=36
nt=72

# QUDA optimization flags
export QUDA_ENABLE_GDR=1
export QUDA_MILC_HISQ_RECONSTRUCT=13
export QUDA_MILC_HISQ_RECONSTRUCT_SLOPPY=9
total_tasks=$SLURM_NTASKS
total_gpus=$( $SLURM_GPUS_PER_TASK * $total_tasks )

module load PrgEnv-gnu
module load cudatoolkit
module load craype-accel-nvidia80
export MPICH_GPU_SUPPORT_ENABLED=1

printf "%s\n" "total_tasks = $SLURM_NTASKS total_gpus = $total_gpus"

# Set defalt problem size
nx=36 
ny=36
nz=36
nt=72

# QUDA optimization flags
export QUDA_ENABLE_GDR=1
export QUDA_MILC_HISQ_RECONSTRUCT=13
export QUDA_MILC_HISQ_RECONSTRUCT_SLOPPY=9

exe=./su3_rhmd_hisq.pm

# decompose in `t` dimension. This is faster on GPUs because provides better coalsceing.
QUDA_DECOMPOSITION="-qmp-geom 1 1 1 4 -qmp-alloc-map 3 2 1 0 -qmp-logic-map 3 2 1 0"

# set the resource path.
export QUDA_RESOURCE_PATH=$PWD/qudatune_pm/"$nx"_"$ny"_"$nz"_"$nt"/$num_nodes"nodes"

if [ ! -d $QUDA_RESOURCE_PATH ]; then
    mkdir -p $QUDA_RESOURCE_PATH
fi

#Set the brief flag to `true` for the first run. 
#It will reduce the value of on parameters such as `steps_per_trajectory` for tuning runs.

build=false
brief=true

dimension=${nx}x${ny}x${nz}x${nt}
checklat=$dimension.chklat
if $build; then
  . milc_in.sh
  warms=40
  trajecs=0
  save_cmd="save_serial $checklat"
  reload_cmd="continue"
  echo "$0: Building lattice of dimension $dimension"
else
  if $brief; then
    echo "$0: USING milc_in_brief.sh"
    . milc_in_brief.sh
  else
    . milc_in.sh
  fi
  warms=0
  trajecs=2
  save_cmd="forget"
  reload_cmd="reload_parallel $checklat"
  echo    "$0: Running MILC with lattice of dimension $dimension"
fi

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export OMP_PLACES=threads
export OMP_PROC_BIND=spread
n=$SLURM_NTASKS

echo -n "$0""$@"; echo -n ": "
echo -n "OMP_PLACES=$OMP_PLACES; "
echo -n "OMP_PROC_BIND=$OMP_PROC_BIND; "
echo -n "OMP_NUM_THREADS=$OMP_NUM_THREADS; "
command="srun -n $n ./numa.sh $exe $QUDA_DECOMPOSITION"
echo "$command"

echo -n "$0: BEGIN TIME: "; date
run_milc
echo -n "$0: END TIME: "; date
