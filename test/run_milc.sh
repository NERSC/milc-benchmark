#!/bin/bash
#SBATCH --account=nstaff
#SBATCH --partition=debug
#SBATCH --constraint="haswell"
#SBATCH --nodes=1
#SBATCH --time=00:30:00
#SBATCH --job-name=milc
num_nodes=$SLURM_JOB_NUM_NODES
c_node=$SLURM_CPUS_ON_NODE
ht_core=2			# number of hyperthreads per core

# Set defalt problem size
nx=16
ny=16
nz=16
nt=16

exe=./su3_rhmd_hisq
ihelp=false
build=false
debug=false
brief=false
r=8			# default number of ranks per node
t=0			# derive threads
while [ "$#" -ge 1 ] ; do
  case "$1" in
    "-h" | "--help" )     ihelp=true; shift 1;;
    "--exe" )             exe=$2; shift 2;;
    "--build" )           build=true; shift 1;;
    "--debug" )           debug=true; shift 1;;
    "--brief" )           brief=true; shift 1;;
    "--full" )            brief=false; shift 1;;
    "-r" | "--ranks" )    r=$2; shift 2;;
    "-t" | "--threads" )  t=$2; shift 2;;
    "-nx" )  		  nx=$2; shift 2;;
    "-ny" )  		  ny=$2; shift 2;;
    "-nz" )  		  nz=$2; shift 2;;
    "-nt" )  		  nt=$2; shift 2;;
    *)                    shift 1; continue;;
  esac
done

if $ihelp; then
  echo "$0 [--help] [--exe executable] [--build] [--debug] [--brief(default) | --full] \
[-r ranks] [-t threads/rank] [-nx nx] [-ny ny] [-nz nz] [-nt nt]"
  exit
fi

dimension=${nx}x${ny}x${nz}x${nt}
checklat=$dimension.chklat
if $build; then
  . milc_in.sh
  warms=40
  trajecs=0
  #save_cmd="save_parallel $checklat" # I've had issues with save_parallel on Cori
  save_cmd="save_serial $checklat"
  reload_cmd="continue"
  echo "$0: Building lattice of dimension $dimension"
else
  if $brief; then
    echo "$0: USING milc_in_brief.sh"
    . milc_in_brief.sh
  else
    . milc_in.sh
  fi
  warms=0
  trajecs=2
  save_cmd="forget"
  reload_cmd="reload_parallel $checklat"
  echo    "$0: Running MILC with lattice of dimension $dimension"
fi

export OMP_PLACES=threads
export OMP_PROC_BIND=spread
n=$(( r * num_nodes ))
c=$(( c_node / r ))     # cores per rank
if [ $t -eq 0 ]; then		# t not set on command line
  t=$(( c / ht_core ))
fi 
export OMP_NUM_THREADS=$t

echo -n "$0""$@"; echo -n ": "
echo -n "OMP_PLACES=$OMP_PLACES; "
echo -n "OMP_PROC_BIND=$OMP_PROC_BIND; "
echo -n "OMP_NUM_THREADS=$OMP_NUM_THREADS; "
command="srun -n $n -c $c --cpu_bind=cores $EXE_PREFIX $exe"
echo "$command"

if $debug; then exit; fi
echo -n "$0: BEGIN TIME: "; date
run_milc
echo -n "$0: END TIME: "; date

